// searchText
export let setSearchText = (searchText) => {
	return {
		type: 'SET_SEARCH_TEXT',
		searchText
	}
};

// showChecked
export let setShowChecked = (value) => {
	return {
		type: 'SET_SHOW_CHECKED',
		value
	}
};

// itemsList
export let setItemsList = (itemsList) => {
	return {
		type: 'SET_ITEMS_LIST',
		itemsList
	}
};

export let startItemsList = () => {
	return (dispatch, getState) => {
		let tempList = [
			{id: 0, text: 'Item uno', checked: false},
			{id: 1, text: 'Item dos', checked: false},
			{id: 2, text: 'Item tres', checked: true},
			{id: 3, text: 'Item cuatro', checked: true}
		];
		dispatch(setItemsList(tempList));
	};
};

export let addItem = (item) => {
	return {
		type: 'ADD_ITEM',
		item
	}
};

export let startAddItem = (text) => {
	return (dispatch, getState) => {
		let newItem = {
			id: (new Date()).getTime(),
			text,
			checked: false
		};
		dispatch(addItem(newItem));
	};
};

export let removeItem = (id) => {
	return {
		type: 'REMOVE_ITEM',
		id
	}
};

export let checkItem = (id, checked) => {
	return {
		type: 'CHECK_ITEM',
		id,
		checked
	}
};