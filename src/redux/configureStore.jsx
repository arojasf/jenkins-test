import * as redux from 'redux';
import thunk from 'redux-thunk';

import {searchTextReducer, showCheckedReducer, itemsListReducer} from 'redux/reducers';

export let configureStore = (initialState = {}) => {
	let reducer = redux.combineReducers({
		searchText: searchTextReducer,
		showChecked: showCheckedReducer,
		itemsList: itemsListReducer
	});

	let store = redux.createStore(reducer, redux.compose(
		redux.applyMiddleware(thunk),
		window.devToolsExtension ? window.devToolsExtension() : f => f
	));

	return store;
};