export let searchTextReducer = (state = '', action) => {
	switch(action.type){
		case 'SET_SEARCH_TEXT':
			return action.searchText;
		default:
			return state;
	}
};

export let showCheckedReducer = (state = true, action) => {
	switch(action.type){
		case 'SET_SHOW_CHECKED':
			return action.value;
		default:
			return state;
	}
};


export let itemsListReducer = (state = [], action) => {
	switch(action.type){
		case 'SET_ITEMS_LIST':
			return action.itemsList;
		case 'ADD_ITEM':
			return [
				...state,
				action.item
			];
		case 'REMOVE_ITEM':
			return state.filter((item) => {
				return (item.id !== action.id);
			});
		case 'CHECK_ITEM':
			return state.map((item) => {
				if(item.id === action.id){
					return {
						...item,
						checked: action.checked
					};
				} else {
					return item;
				}
			});
		default:
			return state;
	}
};