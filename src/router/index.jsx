import React, {Component} from 'react';
import {BrowserRouter, Route} from 'react-router-dom';

import Navigation from 'router/Navigation';
import Home from 'components/Home';
import CompOne from 'components/compOne';
import CompTwo from 'components/compTwo';

class Router extends Component {
	render() {
		return (
			<BrowserRouter>
				<div>
					<div className="header">
						<Navigation/>
					</div>
					<div className="container">
						<Route exact path="/" component={Home}/>
						<Route path="/one" component={CompOne}/>
						<Route path="/two" component={CompTwo}/>
					</div>
				</div>
			</BrowserRouter>
		);
	}
}

export default Router;