import React, {Component} from 'react';
import {NavLink, withRouter} from 'react-router-dom'

class Navigation extends Component {
	render() {
		return (
			<ul>
				<li><NavLink exact to="/">Home</NavLink></li>
				<li><NavLink to="/one">One</NavLink></li>
				<li><NavLink to="/two">Two</NavLink></li>
			</ul>
		);
	}
}

export default withRouter(Navigation);