import React, {Component} from 'react';
import {connect} from 'react-redux';

class CompTwo extends Component{
	countChecked(itemsList){
		let count = 0;
		for (let item of itemsList) {
			if(item.checked){
				count ++;
			}
		}
		return count;
	}

	countUnchecked(itemsList){
		let count = 0;
		for (let item of itemsList) {
			if(!item.checked){
				count ++;
			}
		}
		return count;
	}

	render() {
		let {itemsList, searchText, showChecked} = this.props;
		return (
			<div className="comp-two">
				<h1>CompTwo</h1>
				<ul className="summary-list">
					<li>Items: <div className="result">{itemsList.length}</div></li>
					<li>Checked Items: <div className="result">{this.countChecked(itemsList)}</div></li>
					<li>Unchecked Items: <div className="result">{this.countUnchecked(itemsList)}</div></li>
					<li>Search Text: <div className="result">"{searchText}"</div></li>
					<li>ShowChecked: <div className="result">{showChecked.toString()}</div></li>
				</ul>
			</div>
		);
	}
}

export default connect(
	(state) => {
		return state;
	}
)(CompTwo);