import React, {Component} from 'react';
import {connect} from 'react-redux';

import * as actions from 'redux/actions';

class CompOneFilter extends Component{
	changeSearchText(text){
		this.props.dispatch(actions.setSearchText(text));
	}

	changeShowChecked(value){
		this.props.dispatch(actions.setShowChecked(value));
	}

	render() {
		let {searchText, showChecked} = this.props;
		return (
			<div className="filter">
				<div className="search">
					<input type="search" ref="text" placeholder="Search" value={searchText}
						onChange={() => {this.changeSearchText(this.refs.text.value)}}/>
				</div>
				<div className="show-checked">
					<label>
						Show Checked:
						<input type="checkbox" checked={showChecked}
							onChange={() => {this.changeShowChecked(!showChecked)}}/>
					</label>
				</div>
			</div>
		);
	}
}

export default connect((state) => {
	return {
		searchText: state.searchText,
		showChecked: state.showChecked
	};
})(CompOneFilter);