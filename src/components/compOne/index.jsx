import React, {Component} from 'react';

import Filter from 'components/compOne/Filter';
import Form from 'components/compOne/Form';
import List from 'components/compOne/List';

class CompOne extends Component{
	render() {
		return (
			<div className="comp-one">
				<h1>CompOne</h1>
				<Filter/>
				<List/>
				<Form/>
			</div>
		);
	}
}

export default CompOne;