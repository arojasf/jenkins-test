# Proyecto React Base
holas4

Este proyecto fue creado con [Create React App](https://github.com/facebookincubator/create-react-app).

También utiliza:

* [node-sass-chokidar](https://github.com/michaelwayman/node-sass-chokidar).
* [npm-run-all](https://github.com/mysticatea/npm-run-all).
* [React Router](https://github.com/ReactTraining/react-router).
* [Redux](https://redux.js.org/).
* [React Redux](https://github.com/reactjs/react-redux).
* [Redux Thunk](https://github.com/gaearon/redux-thunk).


## Estructura de archivos


```
proyecto-react-base/
  node_modules/
  public/
    index.html
    favicon.ico
  src/
    api/
    components/
      compOne/
        Filter.jsx
        Form.jsx
        index.jsx
        List.jsx
        ListItem.jsx
      compTwo/
        index.jsx
      Home.jx
    redux/
      actions.jsx
      configureStore.jsx
      reducers.jsx
    router/
      index.jsx
      Navigation.jsx
    styles/
      base/
      components/
      app.scss
    App.jsx
    index.js
  .env
  .gitignore
  package-lock.json
  package.json
  README.md
```

Para que el proyecto se construya, **estos archivos deben existir con este nombre exacto**:

* `public/index.html` es el tamplate de la página;
* `src/index.js` es el punto de entrada de JavaScript.
* `src/styles/app.scss` es el punto de entrada de Sass.

Puedes borrar o modificar los otros archivos.
